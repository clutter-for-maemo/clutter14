# Chinese translations for clutter package
# clutter 软件包的简体中文翻译.
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the clutter package.
# Aron Xu <happyaron.xu@gmail.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: clutter master\n"
"Report-Msgid-Bugs-To: http://bugzilla.clutter-project.org/enter_bug.cgi?"
"product=clutter\n"
"POT-Creation-Date: 2010-11-30 13:09+0000\n"
"PO-Revision-Date: 2010-09-30 15:09+0800\n"
"Last-Translator: Aron Xu <happyaron.xu@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: clutter/clutter-actor.c:3272
msgid "X coordinate"
msgstr "X 坐标"

#: clutter/clutter-actor.c:3273
msgid "X coordinate of the actor"
msgstr ""

#: clutter/clutter-actor.c:3288
msgid "Y coordinate"
msgstr "Y 坐标"

#: clutter/clutter-actor.c:3289
msgid "Y coordinate of the actor"
msgstr ""

#: clutter/clutter-actor.c:3304 clutter/clutter-behaviour-ellipse.c:473
msgid "Width"
msgstr "宽度"

#: clutter/clutter-actor.c:3305
msgid "Width of the actor"
msgstr ""

#: clutter/clutter-actor.c:3319 clutter/clutter-behaviour-ellipse.c:489
msgid "Height"
msgstr "高度"

#: clutter/clutter-actor.c:3320
msgid "Height of the actor"
msgstr ""

#: clutter/clutter-actor.c:3338
msgid "Fixed X"
msgstr "固定 X 坐标"

#: clutter/clutter-actor.c:3339
msgid "Forced X position of the actor"
msgstr ""

#: clutter/clutter-actor.c:3357
msgid "Fixed Y"
msgstr "固定 Y 坐标"

#: clutter/clutter-actor.c:3358
msgid "Forced Y position of the actor"
msgstr ""

#: clutter/clutter-actor.c:3374
msgid "Fixed position set"
msgstr ""

#: clutter/clutter-actor.c:3375
msgid "Whether to use fixed positioning for the actor"
msgstr ""

#: clutter/clutter-actor.c:3397
msgid "Min Width"
msgstr "最小宽度"

#: clutter/clutter-actor.c:3398
msgid "Forced minimum width request for the actor"
msgstr ""

#: clutter/clutter-actor.c:3417
msgid "Min Height"
msgstr "最小高度"

#: clutter/clutter-actor.c:3418
msgid "Forced minimum height request for the actor"
msgstr ""

#: clutter/clutter-actor.c:3437
msgid "Natural Width"
msgstr ""

#: clutter/clutter-actor.c:3438
msgid "Forced natural width request for the actor"
msgstr ""

#: clutter/clutter-actor.c:3457
msgid "Natural Height"
msgstr ""

#: clutter/clutter-actor.c:3458
msgid "Forced natural height request for the actor"
msgstr ""

#: clutter/clutter-actor.c:3474
msgid "Minimum width set"
msgstr ""

#: clutter/clutter-actor.c:3475
msgid "Whether to use the min-width property"
msgstr ""

#: clutter/clutter-actor.c:3490
msgid "Minimum height set"
msgstr ""

#: clutter/clutter-actor.c:3491
msgid "Whether to use the min-height property"
msgstr ""

#: clutter/clutter-actor.c:3506
msgid "Natural width set"
msgstr ""

#: clutter/clutter-actor.c:3507
msgid "Whether to use the natural-width property"
msgstr ""

#: clutter/clutter-actor.c:3524
msgid "Natural height set"
msgstr ""

#: clutter/clutter-actor.c:3525
msgid "Whether to use the natural-height property"
msgstr ""

#: clutter/clutter-actor.c:3544
msgid "Allocation"
msgstr "分配"

#: clutter/clutter-actor.c:3545
msgid "The actor's allocation"
msgstr ""

#: clutter/clutter-actor.c:3601
msgid "Request Mode"
msgstr "请求模式"

#: clutter/clutter-actor.c:3602
msgid "The actor's request mode"
msgstr ""

#: clutter/clutter-actor.c:3617
msgid "Depth"
msgstr "色深"

#: clutter/clutter-actor.c:3618
msgid "Position on the Z axis"
msgstr ""

#: clutter/clutter-actor.c:3632
msgid "Opacity"
msgstr "透明度"

#: clutter/clutter-actor.c:3633
msgid "Opacity of an actor"
msgstr ""

#: clutter/clutter-actor.c:3648
msgid "Visible"
msgstr "可见性"

#: clutter/clutter-actor.c:3649
msgid "Whether the actor is visible or not"
msgstr ""

#: clutter/clutter-actor.c:3664
msgid "Mapped"
msgstr ""

#: clutter/clutter-actor.c:3665
msgid "Whether the actor will be painted"
msgstr ""

#: clutter/clutter-actor.c:3679
msgid "Realized"
msgstr ""

#: clutter/clutter-actor.c:3680
msgid "Whether the actor has been realized"
msgstr ""

#: clutter/clutter-actor.c:3696
msgid "Reactive"
msgstr "重新激活"

#: clutter/clutter-actor.c:3697
msgid "Whether the actor is reactive to events"
msgstr ""

#: clutter/clutter-actor.c:3709
msgid "Has Clip"
msgstr ""

#: clutter/clutter-actor.c:3710
msgid "Whether the actor has a clip set"
msgstr ""

#: clutter/clutter-actor.c:3725
msgid "Clip"
msgstr ""

#: clutter/clutter-actor.c:3726
#, fuzzy
msgid "The clip region for the actor"
msgstr "文本方向"

#: clutter/clutter-actor.c:3740 clutter/clutter-actor-meta.c:186
#: clutter/clutter-binding-pool.c:320 clutter/clutter-input-device.c:148
msgid "Name"
msgstr "名称"

#: clutter/clutter-actor.c:3741
msgid "Name of the actor"
msgstr ""

#: clutter/clutter-actor.c:3755
msgid "Scale X"
msgstr "缩放 X 坐标"

#: clutter/clutter-actor.c:3756
msgid "Scale factor on the X axis"
msgstr ""

#: clutter/clutter-actor.c:3771
msgid "Scale Y"
msgstr "缩放 Y 坐标"

#: clutter/clutter-actor.c:3772
msgid "Scale factor on the Y axis"
msgstr ""

#: clutter/clutter-actor.c:3787
msgid "Scale Center X"
msgstr ""

#: clutter/clutter-actor.c:3788
msgid "Horizontal scale center"
msgstr ""

#: clutter/clutter-actor.c:3803
msgid "Scale Center Y"
msgstr ""

#: clutter/clutter-actor.c:3804
msgid "Vertical scale center"
msgstr ""

#: clutter/clutter-actor.c:3819
msgid "Scale Gravity"
msgstr ""

#: clutter/clutter-actor.c:3820
msgid "The center of scaling"
msgstr ""

#: clutter/clutter-actor.c:3837
msgid "Rotation Angle X"
msgstr ""

#: clutter/clutter-actor.c:3838
msgid "The rotation angle on the X axis"
msgstr ""

#: clutter/clutter-actor.c:3853
msgid "Rotation Angle Y"
msgstr ""

#: clutter/clutter-actor.c:3854
msgid "The rotation angle on the Y axis"
msgstr ""

#: clutter/clutter-actor.c:3869
msgid "Rotation Angle Z"
msgstr ""

#: clutter/clutter-actor.c:3870
msgid "The rotation angle on the Z axis"
msgstr ""

#: clutter/clutter-actor.c:3885
msgid "Rotation Center X"
msgstr ""

#: clutter/clutter-actor.c:3886
msgid "The rotation center on the X axis"
msgstr ""

#: clutter/clutter-actor.c:3902
msgid "Rotation Center Y"
msgstr ""

#: clutter/clutter-actor.c:3903
msgid "The rotation center on the Y axis"
msgstr ""

#: clutter/clutter-actor.c:3919
msgid "Rotation Center Z"
msgstr ""

#: clutter/clutter-actor.c:3920
msgid "The rotation center on the Z axis"
msgstr ""

#: clutter/clutter-actor.c:3936
msgid "Rotation Center Z Gravity"
msgstr ""

#: clutter/clutter-actor.c:3937
msgid "Center point for rotation around the Z axis"
msgstr ""

#: clutter/clutter-actor.c:3955
msgid "Anchor X"
msgstr ""

#: clutter/clutter-actor.c:3956
msgid "X coordinate of the anchor point"
msgstr ""

#: clutter/clutter-actor.c:3972
msgid "Anchor Y"
msgstr ""

#: clutter/clutter-actor.c:3973
msgid "Y coordinate of the anchor point"
msgstr ""

#: clutter/clutter-actor.c:3988
msgid "Anchor Gravity"
msgstr ""

#: clutter/clutter-actor.c:3989
msgid "The anchor point as a ClutterGravity"
msgstr ""

#: clutter/clutter-actor.c:4008
msgid "Show on set parent"
msgstr ""

#: clutter/clutter-actor.c:4009
msgid "Whether the actor is shown when parented"
msgstr ""

#: clutter/clutter-actor.c:4029
msgid "Clip to Allocation"
msgstr ""

#: clutter/clutter-actor.c:4030
msgid "Sets the clip region to track the actor's allocation"
msgstr ""

#: clutter/clutter-actor.c:4040
msgid "Text Direction"
msgstr "文本方向"

#: clutter/clutter-actor.c:4041
msgid "Direction of the text"
msgstr "文本的方向"

#: clutter/clutter-actor.c:4059
msgid "Has Pointer"
msgstr ""

#: clutter/clutter-actor.c:4060
msgid "Whether the actor contains the pointer of an input device"
msgstr ""

#: clutter/clutter-actor.c:4077
msgid "Actions"
msgstr "动作"

#: clutter/clutter-actor.c:4078
msgid "Adds an action to the actor"
msgstr ""

#: clutter/clutter-actor.c:4092
msgid "Constraints"
msgstr ""

#: clutter/clutter-actor.c:4093
msgid "Adds a constraint to the actor"
msgstr ""

#: clutter/clutter-actor-meta.c:171 clutter/clutter-child-meta.c:143
msgid "Actor"
msgstr ""

#: clutter/clutter-actor-meta.c:172
msgid "The actor attached to the meta"
msgstr ""

#: clutter/clutter-actor-meta.c:187
msgid "The name of the meta"
msgstr ""

#: clutter/clutter-actor-meta.c:201 clutter/clutter-shader.c:261
msgid "Enabled"
msgstr "启用"

#: clutter/clutter-actor-meta.c:202
msgid "Whether the meta is enabled"
msgstr ""

#: clutter/clutter-align-constraint.c:238
#: clutter/clutter-bind-constraint.c:290 clutter/clutter-clone.c:292
msgid "Source"
msgstr "来源"

#: clutter/clutter-align-constraint.c:239
msgid "The source of the alignment"
msgstr ""

#: clutter/clutter-align-constraint.c:254
msgid "Align Axis"
msgstr ""

#: clutter/clutter-align-constraint.c:255
msgid "The axis to align the position to"
msgstr ""

#: clutter/clutter-align-constraint.c:276
#: clutter/clutter-desaturate-effect.c:312
msgid "Factor"
msgstr ""

#: clutter/clutter-align-constraint.c:277
msgid "The alignment factor, between 0.0 and 1.0"
msgstr ""

#: clutter/clutter-alpha.c:339 clutter/clutter-animation.c:526
#: clutter/clutter-animator.c:1805
msgid "Timeline"
msgstr "时间轴"

#: clutter/clutter-alpha.c:340
msgid "Timeline used by the alpha"
msgstr "Alpha 使用的时间轴"

#: clutter/clutter-alpha.c:356
msgid "Alpha value"
msgstr "Alpha 值"

#: clutter/clutter-alpha.c:357
msgid "Alpha value as computed by the alpha"
msgstr ""

#: clutter/clutter-alpha.c:378 clutter/clutter-animation.c:480
msgid "Mode"
msgstr "模式"

#: clutter/clutter-alpha.c:379
msgid "Progress mode"
msgstr "进度模式"

#: clutter/clutter-animation.c:463
msgid "Object"
msgstr "对象"

#: clutter/clutter-animation.c:464
msgid "Object to which the animation applies"
msgstr "应用动画的对象"

#: clutter/clutter-animation.c:481
msgid "The mode of the animation"
msgstr "动画的模式"

#: clutter/clutter-animation.c:496 clutter/clutter-animator.c:1788
#: clutter/clutter-media.c:196 clutter/clutter-state.c:1199
msgid "Duration"
msgstr "时长"

#: clutter/clutter-animation.c:497
msgid "Duration of the animation, in milliseconds"
msgstr "动画时长，以毫秒计"

#: clutter/clutter-animation.c:511
msgid "Loop"
msgstr "循环"

#: clutter/clutter-animation.c:512
msgid "Whether the animation should loop"
msgstr "动画是否循环"

#: clutter/clutter-animation.c:527
msgid "The timeline used by the animation"
msgstr "动画使用的时间轴"

#: clutter/clutter-animation.c:541 clutter/clutter-behaviour.c:308
msgid "Alpha"
msgstr "Alpha"

#: clutter/clutter-animation.c:542
msgid "The alpha used by the animation"
msgstr "动画使用的 alpha"

#: clutter/clutter-animator.c:1789
msgid "The duration of the animation"
msgstr "动画的时长"

#: clutter/clutter-animator.c:1806
msgid "The timeline of the animation"
msgstr "动画的时间轴"

#: clutter/clutter-behaviour.c:309
msgid "Alpha Object to drive the behaviour"
msgstr ""

#: clutter/clutter-behaviour-depth.c:170
msgid "Start Depth"
msgstr "起始色深"

#: clutter/clutter-behaviour-depth.c:171
msgid "Initial depth to apply"
msgstr "应用的初始色深"

#: clutter/clutter-behaviour-depth.c:184
msgid "End Depth"
msgstr "终点色深"

#: clutter/clutter-behaviour-depth.c:185
msgid "Final depth to apply"
msgstr "应用的终点色深"

#: clutter/clutter-behaviour-ellipse.c:393
msgid "Start Angle"
msgstr "起始角度"

#: clutter/clutter-behaviour-ellipse.c:394
#: clutter/clutter-behaviour-rotate.c:278
msgid "Initial angle"
msgstr "初始的角度"

#: clutter/clutter-behaviour-ellipse.c:409
msgid "End Angle"
msgstr "终点角度"

#: clutter/clutter-behaviour-ellipse.c:410
#: clutter/clutter-behaviour-rotate.c:296
msgid "Final angle"
msgstr "终点的角度"

#: clutter/clutter-behaviour-ellipse.c:425
msgid "Angle x tilt"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:426
msgid "Tilt of the ellipse around x axis"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:441
msgid "Angle y tilt"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:442
msgid "Tilt of the ellipse around y axis"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:457
msgid "Angle z tilt"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:458
msgid "Tilt of the ellipse around z axis"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:474
msgid "Width of the ellipse"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:490
msgid "Height of ellipse"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:505
msgid "Center"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:506
msgid "Center of ellipse"
msgstr ""

#: clutter/clutter-behaviour-ellipse.c:520
#: clutter/clutter-behaviour-rotate.c:331
msgid "Direction"
msgstr "方向"

#: clutter/clutter-behaviour-ellipse.c:521
#: clutter/clutter-behaviour-rotate.c:332
#, fuzzy
msgid "Direction of rotation"
msgstr "文本方向"

#: clutter/clutter-behaviour-opacity.c:184
msgid "Opacity Start"
msgstr ""

#: clutter/clutter-behaviour-opacity.c:185
msgid "Initial opacity level"
msgstr ""

#: clutter/clutter-behaviour-opacity.c:200
msgid "Opacity End"
msgstr ""

#: clutter/clutter-behaviour-opacity.c:201
msgid "Final opacity level"
msgstr ""

#: clutter/clutter-behaviour-path.c:217
msgid "Path"
msgstr "路径"

#: clutter/clutter-behaviour-path.c:218
msgid "The ClutterPath object representing the path to animate along"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:277
msgid "Angle Begin"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:295
msgid "Angle End"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:313
msgid "Axis"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:314
msgid "Axis of rotation"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:349
msgid "Center X"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:350
msgid "X coordinate of the center of rotation"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:367
msgid "Center Y"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:368
msgid "Y coordinate of the center of rotation"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:385
msgid "Center Z"
msgstr ""

#: clutter/clutter-behaviour-rotate.c:386
msgid "Z coordinate of the center of rotation"
msgstr ""

#: clutter/clutter-behaviour-scale.c:216
msgid "X Start Scale"
msgstr ""

#: clutter/clutter-behaviour-scale.c:217
msgid "Initial scale on the X axis"
msgstr ""

#: clutter/clutter-behaviour-scale.c:233
msgid "X End Scale"
msgstr ""

#: clutter/clutter-behaviour-scale.c:234
msgid "Final scale on the X axis"
msgstr ""

#: clutter/clutter-behaviour-scale.c:250
msgid "Y Start Scale"
msgstr ""

#: clutter/clutter-behaviour-scale.c:251
msgid "Initial scale on the Y axis"
msgstr ""

#: clutter/clutter-behaviour-scale.c:267
msgid "Y End Scale"
msgstr ""

#: clutter/clutter-behaviour-scale.c:268
msgid "Final scale on the Y axis"
msgstr ""

#: clutter/clutter-bind-constraint.c:291
msgid "The source of the binding"
msgstr ""

#: clutter/clutter-bind-constraint.c:306
msgid "Coordinate"
msgstr ""

#: clutter/clutter-bind-constraint.c:307
msgid "The coordinate to bind"
msgstr ""

#: clutter/clutter-bind-constraint.c:323
msgid "Offset"
msgstr "偏移"

#: clutter/clutter-bind-constraint.c:324
msgid "The offset in pixels to apply to the binding"
msgstr ""

#: clutter/clutter-bin-layout.c:258 clutter/clutter-bin-layout.c:583
#: clutter/clutter-box-layout.c:409
msgid "Horizontal Alignment"
msgstr ""

#: clutter/clutter-bin-layout.c:259
msgid "Horizontal alignment for the actor inside the layer"
msgstr ""

#: clutter/clutter-bin-layout.c:270 clutter/clutter-bin-layout.c:601
#: clutter/clutter-box-layout.c:418
msgid "Vertical Alignment"
msgstr ""

#: clutter/clutter-bin-layout.c:271
msgid "Vertical alignment for the actor inside the layer manager"
msgstr ""

#: clutter/clutter-bin-layout.c:584
msgid "Default horizontal alignment for the actors inside the layout manager"
msgstr ""

#: clutter/clutter-bin-layout.c:602
msgid "Default vertical alignment for the actors inside the layout manager"
msgstr ""

#: clutter/clutter-binding-pool.c:321
msgid "The unique name of the binding pool"
msgstr ""

#: clutter/clutter-box.c:494
msgid "Layout Manager"
msgstr "样式管理器"

#: clutter/clutter-box.c:495
msgid "The layout manager used by the box"
msgstr ""

#: clutter/clutter-box.c:514 clutter/clutter-rectangle.c:247
#: clutter/clutter-stage.c:1179
msgid "Color"
msgstr "色彩"

#: clutter/clutter-box.c:515
msgid "The background color of the box"
msgstr "框的背景颜色"

#: clutter/clutter-box.c:529
msgid "Color Set"
msgstr ""

#: clutter/clutter-box.c:530
msgid "Whether the background color is set"
msgstr ""

#: clutter/clutter-box-layout.c:384
msgid "Expand"
msgstr "展开"

#: clutter/clutter-box-layout.c:385
msgid "Allocate extra space for the child"
msgstr ""

#: clutter/clutter-box-layout.c:391
msgid "Horizontal Fill"
msgstr "水平填充"

#: clutter/clutter-box-layout.c:392
msgid ""
"Whether the child should receive priority when the container is allocating "
"spare space on the horizontal axis"
msgstr ""

#: clutter/clutter-box-layout.c:400
msgid "Vertical Fill"
msgstr "竖直填充"

#: clutter/clutter-box-layout.c:401
msgid ""
"Whether the child should receive priority when the container is allocating "
"spare space on the vertical axis"
msgstr ""

#: clutter/clutter-box-layout.c:410
msgid "Horizontal alignment of the actor within the cell"
msgstr ""

#: clutter/clutter-box-layout.c:419
msgid "Vertical alignment of the actor within the cell"
msgstr ""

#: clutter/clutter-box-layout.c:1090
msgid "Vertical"
msgstr "竖直"

#: clutter/clutter-box-layout.c:1091
msgid "Whether the layout should be vertical, ratherthan horizontal"
msgstr ""

#: clutter/clutter-box-layout.c:1106 clutter/clutter-flow-layout.c:899
msgid "Homogeneous"
msgstr ""

#: clutter/clutter-box-layout.c:1107
msgid ""
"Whether the layout should be homogeneous, i.e.all childs get the same size"
msgstr ""

#: clutter/clutter-box-layout.c:1122
msgid "Pack Start"
msgstr ""

#: clutter/clutter-box-layout.c:1123
msgid "Whether to pack items at the start of the box"
msgstr ""

#: clutter/clutter-box-layout.c:1136
msgid "Spacing"
msgstr ""

#: clutter/clutter-box-layout.c:1137
msgid "Spacing between children"
msgstr ""

#: clutter/clutter-box-layout.c:1151
msgid "Use Animations"
msgstr "使用动画"

#: clutter/clutter-box-layout.c:1152
msgid "Whether layout changes should be animated"
msgstr ""

#: clutter/clutter-box-layout.c:1173
msgid "Easing Mode"
msgstr ""

#: clutter/clutter-box-layout.c:1174
msgid "The easing mode of the animations"
msgstr ""

#: clutter/clutter-box-layout.c:1191
msgid "Easing Duration"
msgstr ""

#: clutter/clutter-box-layout.c:1192
msgid "The duration of the animations"
msgstr ""

#: clutter/clutter-cairo-texture.c:397
msgid "Surface Width"
msgstr "表明宽度"

#: clutter/clutter-cairo-texture.c:398
msgid "The width of the Cairo surface"
msgstr "Cairo 表明的宽度"

#: clutter/clutter-cairo-texture.c:415
msgid "Surface Height"
msgstr "表明高度"

#: clutter/clutter-cairo-texture.c:416
msgid "The height of the Cairo surface"
msgstr "Cairo 表明的高度"

#: clutter/clutter-child-meta.c:127
msgid "Container"
msgstr "容器"

#: clutter/clutter-child-meta.c:128
msgid "The container that created this data"
msgstr ""

#: clutter/clutter-child-meta.c:144
msgid "The actor wrapped by this data"
msgstr ""

#: clutter/clutter-click-action.c:283
msgid "Pressed"
msgstr "按下时"

#: clutter/clutter-click-action.c:284
msgid "Whether the clickable should be in pressed state"
msgstr ""

#: clutter/clutter-click-action.c:298
msgid "Held"
msgstr "保持时"

#: clutter/clutter-click-action.c:299
msgid "Whether the clickable has a grab"
msgstr ""

#: clutter/clutter-clone.c:293
msgid "Specifies the actor to be cloned"
msgstr ""

#: clutter/clutter-colorize-effect.c:310
msgid "Tint"
msgstr ""

#: clutter/clutter-colorize-effect.c:311
msgid "The tint to apply"
msgstr ""

#: clutter/clutter-deform-effect.c:525
msgid "Horiontal Tiles"
msgstr ""

#: clutter/clutter-deform-effect.c:526
msgid "The number of horizontal tiles"
msgstr ""

#: clutter/clutter-deform-effect.c:542
msgid "Vertical Tiles"
msgstr ""

#: clutter/clutter-deform-effect.c:543
msgid "The number of vertical tiles"
msgstr ""

#: clutter/clutter-deform-effect.c:561
msgid "Back Material"
msgstr ""

#: clutter/clutter-deform-effect.c:562
msgid "The material to be used when painting the back of the actor"
msgstr ""

#: clutter/clutter-desaturate-effect.c:313
msgid "The desaturation factor"
msgstr ""

#: clutter/clutter-device-manager.c:132
msgid "Backend"
msgstr "后端"

#: clutter/clutter-device-manager.c:133
msgid "The ClutterBackend of the device manager"
msgstr ""

#: clutter/clutter-drag-action.c:494
msgid "Horizontal Drag Threshold"
msgstr ""

#: clutter/clutter-drag-action.c:495
msgid "The horizontal amount of pixels required to start dragging"
msgstr ""

#: clutter/clutter-drag-action.c:515
msgid "Vertical Drag Threshold"
msgstr ""

#: clutter/clutter-drag-action.c:516
msgid "The vertical amount of pixels required to start dragging"
msgstr ""

#: clutter/clutter-drag-action.c:538
msgid "Drag Handle"
msgstr ""

#: clutter/clutter-drag-action.c:539
msgid "The actor that is being dragged"
msgstr ""

#: clutter/clutter-drag-action.c:553
msgid "Drag Axis"
msgstr ""

#: clutter/clutter-drag-action.c:554
msgid "Constraints the dragging to an axis"
msgstr ""

#: clutter/clutter-flow-layout.c:882
msgid "Orientation"
msgstr ""

#: clutter/clutter-flow-layout.c:883
msgid "The orientation of the layout"
msgstr ""

#: clutter/clutter-flow-layout.c:900
msgid "Whether each item should receive the same allocation"
msgstr ""

#: clutter/clutter-flow-layout.c:915
msgid "Column Spacing"
msgstr ""

#: clutter/clutter-flow-layout.c:916
msgid "The spacing between columns"
msgstr ""

#: clutter/clutter-flow-layout.c:934
msgid "Row Spacing"
msgstr ""

#: clutter/clutter-flow-layout.c:935
msgid "The spacing between rows"
msgstr ""

#: clutter/clutter-flow-layout.c:951
msgid "Minimum Column Width"
msgstr "最小列宽"

#: clutter/clutter-flow-layout.c:952
msgid "Minimum width for each column"
msgstr "每列的最小宽度"

#: clutter/clutter-flow-layout.c:969
msgid "Maximum Column Width"
msgstr "最大列宽"

#: clutter/clutter-flow-layout.c:970
msgid "Maximum width for each column"
msgstr "每列的最大宽度"

#: clutter/clutter-flow-layout.c:986
msgid "Minimum Row Height"
msgstr "最小行高"

#: clutter/clutter-flow-layout.c:987
msgid "Minimum height for each row"
msgstr "每行的最小高度"

#: clutter/clutter-flow-layout.c:1004
msgid "Maximum Row Height"
msgstr "最大行高"

#: clutter/clutter-flow-layout.c:1005
msgid "Maximum height for each row"
msgstr "每行的最大高度"

#: clutter/clutter-input-device.c:131
msgid "Id"
msgstr "ID"

#: clutter/clutter-input-device.c:132
msgid "Unique identifier of the device"
msgstr "设备识别符"

#: clutter/clutter-input-device.c:149
msgid "The name of the device"
msgstr "设备名称"

#: clutter/clutter-input-device.c:164
msgid "Device Type"
msgstr "设备类型"

#: clutter/clutter-input-device.c:165
msgid "The type of the device"
msgstr "设备的类型"

#: clutter/clutter-interval.c:414
msgid "Value Type"
msgstr "值类型"

#: clutter/clutter-interval.c:415
msgid "The type of the values in the interval"
msgstr ""

#: clutter/clutter-layout-meta.c:117
msgid "Manager"
msgstr "管理器"

#: clutter/clutter-layout-meta.c:118
msgid "The manager that created this data"
msgstr "创建此数据的管理器"

#: clutter/clutter-main.c:777
msgid "default:LTR"
msgstr "default:LTR"

#: clutter/clutter-main.c:1672
msgid "Show frames per second"
msgstr "显示帧速率"

#: clutter/clutter-main.c:1674
msgid "Default frame rate"
msgstr "默认帧率"

#: clutter/clutter-main.c:1676
msgid "Make all warnings fatal"
msgstr "视所有警告为致命错误"

#: clutter/clutter-main.c:1679
msgid "Direction for the text"
msgstr "文本方向"

#: clutter/clutter-main.c:1682
msgid "Disable mipmapping on text"
msgstr "在文本上禁用 MIP 映射"

#: clutter/clutter-main.c:1685
msgid "Use 'fuzzy' picking"
msgstr "使用模糊选取"

#: clutter/clutter-main.c:1688
msgid "Clutter debugging flags to set"
msgstr "要设置的 Clutter 调试标志"

#: clutter/clutter-main.c:1690
msgid "Clutter debugging flags to unset"
msgstr "要取消设置的 Clutter 调试标志"

#: clutter/clutter-main.c:1694
msgid "Clutter profiling flags to set"
msgstr "要设置的 Clutter 性能分析标志"

#: clutter/clutter-main.c:1696
msgid "Clutter profiling flags to unset"
msgstr "要取消设置的 Clutter 性能分析标志"

#: clutter/clutter-main.c:1699
msgid "Enable accessibility"
msgstr "启用辅助功能"

#: clutter/clutter-main.c:1886
msgid "Clutter Options"
msgstr "Clutter 选项"

#: clutter/clutter-main.c:1887
msgid "Show Clutter Options"
msgstr "显示 Clutter 选项"

#: clutter/clutter-media.c:79
msgid "URI"
msgstr "URI"

#: clutter/clutter-media.c:80
msgid "URI of a media file"
msgstr "媒体文件的 URI"

#: clutter/clutter-media.c:93
msgid "Playing"
msgstr "正在播放"

#: clutter/clutter-media.c:94
msgid "Wheter the actor is playing"
msgstr ""

#: clutter/clutter-media.c:108
msgid "Progress"
msgstr "进度"

#: clutter/clutter-media.c:109
msgid "Current progress of the playback"
msgstr ""

#: clutter/clutter-media.c:122
msgid "Subtitle URI"
msgstr "字幕 URI"

#: clutter/clutter-media.c:123
msgid "URI of a subtitle file"
msgstr "字幕文件的 URI"

#: clutter/clutter-media.c:138
msgid "Subtitle Font Name"
msgstr "字幕字体名称"

#: clutter/clutter-media.c:139
msgid "The font used to display subtitles"
msgstr "显示字幕时使用的字体"

#: clutter/clutter-media.c:153
msgid "Audio Volume"
msgstr "音频音量"

#: clutter/clutter-media.c:154
msgid "The volume of the audio"
msgstr "音频的音量"

#: clutter/clutter-media.c:167
msgid "Can Seek"
msgstr ""

#: clutter/clutter-media.c:168
msgid "Whether the current stream is seekable"
msgstr ""

#: clutter/clutter-media.c:182
msgid "Buffer Fill"
msgstr ""

#: clutter/clutter-media.c:183
msgid "The fill level of the buffer"
msgstr ""

#: clutter/clutter-media.c:197
msgid "The duration of the stream, in seconds"
msgstr ""

#: clutter/clutter-rectangle.c:248
msgid "The color of the rectangle"
msgstr ""

#: clutter/clutter-rectangle.c:261
msgid "Border Color"
msgstr "边界色彩"

#: clutter/clutter-rectangle.c:262
msgid "The color of the border of the rectangle"
msgstr ""

#: clutter/clutter-rectangle.c:277
msgid "Border Width"
msgstr "边界宽度"

#: clutter/clutter-rectangle.c:278
msgid "The width of the border of the rectangle"
msgstr ""

#: clutter/clutter-rectangle.c:292
msgid "Has Border"
msgstr "带有边界"

#: clutter/clutter-rectangle.c:293
msgid "Whether the rectangle should have a border"
msgstr ""

#: clutter/clutter-script.c:404
msgid "Filename Set"
msgstr ""

#: clutter/clutter-script.c:405
msgid "Whether the :filename property is set"
msgstr ""

#: clutter/clutter-script.c:420 clutter/clutter-texture.c:1048
msgid "Filename"
msgstr "文件名"

#: clutter/clutter-script.c:421
msgid "The path of the currently parsed file"
msgstr ""

#: clutter/clutter-shader.c:215
#, fuzzy
msgid "Vertex Source"
msgstr "顶点着色引擎"

#: clutter/clutter-shader.c:216
#, fuzzy
msgid "Source of vertex shader"
msgstr "顶点着色引擎"

#: clutter/clutter-shader.c:230
#, fuzzy
msgid "Fragment Source"
msgstr "片段着色引擎"

#: clutter/clutter-shader.c:231
#, fuzzy
msgid "Source of fragment shader"
msgstr "片段着色引擎"

#: clutter/clutter-shader.c:246
msgid "Compiled"
msgstr ""

#: clutter/clutter-shader.c:247
msgid "Whether the shader is compiled and linked"
msgstr ""

#: clutter/clutter-shader.c:262
msgid "Whether the shader is enabled"
msgstr ""

#: clutter/clutter-shader.c:467
#, c-format
msgid "%s compilation failed: %s"
msgstr "%s 编译失败：%s"

#: clutter/clutter-shader.c:468
msgid "Vertex shader"
msgstr "顶点着色引擎"

#: clutter/clutter-shader.c:469
msgid "Fragment shader"
msgstr "片段着色引擎"

#: clutter/clutter-shader-effect.c:409
msgid "Shader Type"
msgstr "着色引擎类型"

#: clutter/clutter-shader-effect.c:410
msgid "The type of shader used"
msgstr "使用的着色引擎类型"

#: clutter/clutter-stage.c:1121
msgid "Fullscreen Set"
msgstr ""

#: clutter/clutter-stage.c:1122
msgid "Whether the main stage is fullscreen"
msgstr ""

#: clutter/clutter-stage.c:1138
msgid "Offscreen"
msgstr ""

#: clutter/clutter-stage.c:1139
msgid "Whether the main stage should be rendered offscreen"
msgstr ""

#: clutter/clutter-stage.c:1151 clutter/clutter-text.c:2630
msgid "Cursor Visible"
msgstr ""

#: clutter/clutter-stage.c:1152
msgid "Whether the mouse pointer is visible on the main stage"
msgstr ""

#: clutter/clutter-stage.c:1166
msgid "User Resizable"
msgstr "用户可改变大小"

#: clutter/clutter-stage.c:1167
msgid "Whether the stage is able to be resized via user interaction"
msgstr ""

#: clutter/clutter-stage.c:1180
msgid "The color of the stage"
msgstr ""

#: clutter/clutter-stage.c:1194
msgid "Perspective"
msgstr ""

#: clutter/clutter-stage.c:1195
msgid "Perspective projection parameters"
msgstr ""

#: clutter/clutter-stage.c:1210
msgid "Title"
msgstr "标题"

#: clutter/clutter-stage.c:1211
msgid "Stage Title"
msgstr ""

#: clutter/clutter-stage.c:1226
msgid "Use Fog"
msgstr ""

#: clutter/clutter-stage.c:1227
msgid "Whether to enable depth cueing"
msgstr ""

#: clutter/clutter-stage.c:1241
msgid "Fog"
msgstr ""

#: clutter/clutter-stage.c:1242
msgid "Settings for the depth cueing"
msgstr ""

#: clutter/clutter-stage.c:1258
msgid "Use Alpha"
msgstr "使用 Alpha"

#: clutter/clutter-stage.c:1259
msgid "Whether to honour the alpha component of the stage color"
msgstr ""

#: clutter/clutter-stage.c:1275
msgid "Key Focus"
msgstr ""

#: clutter/clutter-stage.c:1276
msgid "The currently key focused actor"
msgstr ""

#: clutter/clutter-stage.c:1292
msgid "No Clear Hint"
msgstr ""

#: clutter/clutter-stage.c:1293
msgid "Whether the stage should clear its contents"
msgstr ""

#: clutter/clutter-state.c:1185
msgid "State"
msgstr ""

#: clutter/clutter-state.c:1186
msgid "Currently set state, (transition to this state might not be complete)"
msgstr ""

#: clutter/clutter-state.c:1200
#, fuzzy
msgid "Default transition duration"
msgstr "默认帧率"

#: clutter/clutter-text.c:2517
msgid "Font Name"
msgstr "字体名称"

#: clutter/clutter-text.c:2518
msgid "The font to be used by the text"
msgstr "文本使用的字体"

#: clutter/clutter-text.c:2535
msgid "Font Description"
msgstr "字体描述"

#: clutter/clutter-text.c:2536
msgid "The font description to be used"
msgstr "使用的字体描述"

#: clutter/clutter-text.c:2552
msgid "Text"
msgstr "文本"

#: clutter/clutter-text.c:2553
msgid "The text to render"
msgstr "要渲染的文本"

#: clutter/clutter-text.c:2567
msgid "Font Color"
msgstr "字体颜色"

#: clutter/clutter-text.c:2568
msgid "Color of the font used by the text"
msgstr "文本字体使用的颜色"

#: clutter/clutter-text.c:2582
msgid "Editable"
msgstr "可编辑"

#: clutter/clutter-text.c:2583
msgid "Whether the text is editable"
msgstr "文本是否可以编辑"

#: clutter/clutter-text.c:2598
msgid "Selectable"
msgstr "可选择"

#: clutter/clutter-text.c:2599
msgid "Whether the text is selectable"
msgstr "文本是否可以选择"

#: clutter/clutter-text.c:2613
msgid "Activatable"
msgstr "可激活"

#: clutter/clutter-text.c:2614
msgid "Whether pressing return causes the activate signal to be emitted"
msgstr ""

#: clutter/clutter-text.c:2631
msgid "Whether the input cursor is visible"
msgstr ""

#: clutter/clutter-text.c:2645 clutter/clutter-text.c:2646
msgid "Cursor Color"
msgstr "指针颜色"

#: clutter/clutter-text.c:2660
msgid "Cursor Color Set"
msgstr ""

#: clutter/clutter-text.c:2661
msgid "Whether the cursor color has been set"
msgstr ""

#: clutter/clutter-text.c:2676
msgid "Cursor Size"
msgstr "指针大小"

#: clutter/clutter-text.c:2677
msgid "The width of the cursor, in pixels"
msgstr "指针的宽度，以像素计"

#: clutter/clutter-text.c:2691
msgid "Position"
msgstr "位置"

#: clutter/clutter-text.c:2692
msgid "The cursor position"
msgstr "指针位置"

#: clutter/clutter-text.c:2707
msgid "Selection-bound"
msgstr "选区边界"

#: clutter/clutter-text.c:2708
msgid "The cursor position of the other end of the selection"
msgstr ""

#: clutter/clutter-text.c:2723 clutter/clutter-text.c:2724
msgid "Selection Color"
msgstr "选区颜色"

#: clutter/clutter-text.c:2738
msgid "Selection Color Set"
msgstr ""

#: clutter/clutter-text.c:2739
msgid "Whether the selection color has been set"
msgstr ""

#: clutter/clutter-text.c:2754
msgid "Attributes"
msgstr "属性"

#: clutter/clutter-text.c:2755
msgid "A list of style attributes to apply to the contents of the actor"
msgstr ""

#: clutter/clutter-text.c:2777
msgid "Use markup"
msgstr "使用标记"

#: clutter/clutter-text.c:2778
msgid "Whether or not the text includes Pango markup"
msgstr ""

#: clutter/clutter-text.c:2794
msgid "Line wrap"
msgstr "换行"

#: clutter/clutter-text.c:2795
msgid "If set, wrap the lines if the text becomes too wide"
msgstr ""

#: clutter/clutter-text.c:2810
msgid "Line wrap mode"
msgstr "换行模式"

#: clutter/clutter-text.c:2811
msgid "Control how line-wrapping is done"
msgstr "控制换行行为"

#: clutter/clutter-text.c:2826
msgid "Ellipsize"
msgstr ""

#: clutter/clutter-text.c:2827
msgid "The preferred place to ellipsize the string"
msgstr ""

#: clutter/clutter-text.c:2843
msgid "Line Alignment"
msgstr ""

#: clutter/clutter-text.c:2844
msgid "The preferred alignment for the string, for multi-line text"
msgstr ""

#: clutter/clutter-text.c:2860
msgid "Justify"
msgstr ""

#: clutter/clutter-text.c:2861
msgid "Whether the text should be justified"
msgstr ""

#: clutter/clutter-text.c:2876
msgid "Password Character"
msgstr "密码字符"

#: clutter/clutter-text.c:2877
msgid "If non-zero, use this character to display the actor's contents"
msgstr ""

#: clutter/clutter-text.c:2891
msgid "Max Length"
msgstr "最大长度"

#: clutter/clutter-text.c:2892
msgid "Maximum length of the text inside the actor"
msgstr ""

#: clutter/clutter-text.c:2915
msgid "Single Line Mode"
msgstr "单行模式"

#: clutter/clutter-text.c:2916
msgid "Whether the text should be a single line"
msgstr "文本是否只应使用一行"

#: clutter/clutter-texture.c:963
msgid "Sync size of actor"
msgstr ""

#: clutter/clutter-texture.c:964
msgid "Auto sync size of actor to underlying pixbuf dimensions"
msgstr ""

#: clutter/clutter-texture.c:971
msgid "Disable Slicing"
msgstr ""

#: clutter/clutter-texture.c:972
msgid ""
"Forces the underlying texture to be singlular and not made of of smaller "
"space saving inidivual textures."
msgstr ""

#: clutter/clutter-texture.c:980
msgid "Tile Waste"
msgstr ""

#: clutter/clutter-texture.c:981
msgid "Maximum waste area of a sliced texture"
msgstr ""

#: clutter/clutter-texture.c:989
msgid "Horizontal repeat"
msgstr ""

#: clutter/clutter-texture.c:990
msgid "Repeat the contents rather than scaling them horizontally."
msgstr ""

#: clutter/clutter-texture.c:997
msgid "Vertical repeat"
msgstr ""

#: clutter/clutter-texture.c:998
msgid "Repeat the contents rather than scaling them vertically."
msgstr ""

#: clutter/clutter-texture.c:1005
msgid "Filter Quality"
msgstr "过滤器质量"

#: clutter/clutter-texture.c:1006
msgid "Rendering quality used when drawing the texture."
msgstr ""

#: clutter/clutter-texture.c:1014
msgid "Pixel Format"
msgstr "像素格式"

#: clutter/clutter-texture.c:1015
msgid "The Cogl pixel format to use."
msgstr ""

#: clutter/clutter-texture.c:1023
msgid "COGL Texture"
msgstr ""

#: clutter/clutter-texture.c:1024
msgid "The underlying COGL texture handle used to draw this actor"
msgstr ""

#: clutter/clutter-texture.c:1031
msgid "COGL Material"
msgstr ""

#: clutter/clutter-texture.c:1032
msgid "The underlying COGL material handle used to draw this actor"
msgstr ""

#: clutter/clutter-texture.c:1049
msgid "The path of the file containing the image data"
msgstr ""

#: clutter/clutter-texture.c:1056
msgid "Keep Aspect Ratio"
msgstr ""

#: clutter/clutter-texture.c:1057
msgid ""
"Keep the aspect ratio of the texture when requesting the preferred width or "
"height"
msgstr ""

#: clutter/clutter-texture.c:1083
msgid "Load asynchronously"
msgstr "同步加载"

#: clutter/clutter-texture.c:1084
msgid ""
"Load files inside a thread to avoid blocking when loading images from disk."
msgstr ""

#: clutter/clutter-texture.c:1100
msgid "Load data asynchronously"
msgstr "同步加载数据"

#: clutter/clutter-texture.c:1101
msgid ""
"Decode image data files inside a thread to reduce blocking when loading "
"images from disk."
msgstr ""

#: clutter/clutter-texture.c:1125
msgid "Pick With Alpha"
msgstr ""

#: clutter/clutter-texture.c:1126
msgid "Shape actor with alpha channel when picking"
msgstr ""

#: clutter/glx/clutter-backend-glx.c:127
msgid "VBlank method to be used (none, dri or glx)"
msgstr "要使用的 VBlank 方式(none、dir 或 glx)"

#: clutter/x11/clutter-backend-x11.c:388
msgid "X display to use"
msgstr "要使用的 X 显示"

#: clutter/x11/clutter-backend-x11.c:394
msgid "X screen to use"
msgstr "要使用的 X 屏幕"

#: clutter/x11/clutter-backend-x11.c:399
msgid "Make X calls synchronous"
msgstr "使 X 调用同步"

#: clutter/x11/clutter-backend-x11.c:406
msgid "Enable XInput support"
msgstr "启用 XInput 支持"

#: clutter/x11/clutter-x11-texture-pixmap.c:510
msgid "Pixmap"
msgstr "位图"

#: clutter/x11/clutter-x11-texture-pixmap.c:511
msgid "The X11 Pixmap to be bound"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:519
msgid "Pixmap width"
msgstr "位图宽度"

#: clutter/x11/clutter-x11-texture-pixmap.c:520
msgid "The width of the pixmap bound to this texture"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:528
msgid "Pixmap height"
msgstr "位图高度"

#: clutter/x11/clutter-x11-texture-pixmap.c:529
msgid "The height of the pixmap bound to this texture"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:537
msgid "Pixmap Depth"
msgstr "位图色深"

#: clutter/x11/clutter-x11-texture-pixmap.c:538
msgid "The depth (in number of bits) of the pixmap bound to this texture"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:546
msgid "Automatic Updates"
msgstr "自动更新"

#: clutter/x11/clutter-x11-texture-pixmap.c:547
msgid "If the texture should be kept in sync with any pixmap changes."
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:555
msgid "Window"
msgstr "窗口"

#: clutter/x11/clutter-x11-texture-pixmap.c:556
msgid "The X11 Window to be bound"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:564
msgid "Window Redirect Automatic"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:565
msgid "If composite window redirects are set to Automatic (or Manual if false)"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:575
msgid "Window Mapped"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:576
msgid "If window is mapped"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:585
msgid "Destroyed"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:586
msgid "If window has been destroyed"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:594
msgid "Window X"
msgstr "窗口 X 坐标"

#: clutter/x11/clutter-x11-texture-pixmap.c:595
msgid "X position of window on screen according to X11"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:603
msgid "Window Y"
msgstr "窗口 Y 坐标"

#: clutter/x11/clutter-x11-texture-pixmap.c:604
msgid "Y position of window on screen according to X11"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:611
msgid "Window Override Redirect"
msgstr ""

#: clutter/x11/clutter-x11-texture-pixmap.c:612
msgid "If this is an override-redirect window"
msgstr ""
