#include <config.h>

#include <stdlib.h>
#include <gmodule.h>
#include <clutter/clutter.h>

#ifdef HAVE_CLUTTER_GLX
#include <clutter/x11/clutter-x11.h>
#endif /* HAVE_CLUTTER_GLX */

static gboolean
accept_focus_clicked_cb (ClutterStage *stage)
{
#ifdef HAVE_CLUTTER_GLX
  clutter_x11_set_stage_accept_focus(stage, !clutter_x11_get_stage_accept_focus(stage));
#endif /* HAVE_CLUTTER_GLX */

  return TRUE;
}

G_MODULE_EXPORT int
test_stage_accept_focus_main (int argc, char *argv[])
{
  gfloat width;
  ClutterActor *stage, *rect, *label, *text;

  clutter_init (&argc, &argv);

  stage = clutter_stage_get_default ();
  ClutterColor _CLUTTER_COLOR_LightScarletRed = { 204,0,0, 255 };

  label = clutter_text_new_with_text ("Sans 16", "Toggle accept focus");
  rect = clutter_rectangle_new_with_color (&_CLUTTER_COLOR_LightScarletRed);
  clutter_actor_set_size (rect,
                          clutter_actor_get_width (label) + 20,
                          clutter_actor_get_height (label) + 20);
  clutter_container_add (CLUTTER_CONTAINER (stage), rect, label, NULL);
  clutter_actor_set_position (label, 10, 10);
  clutter_actor_set_reactive (rect, TRUE);
  g_signal_connect_swapped (rect, "button-press-event",
                            G_CALLBACK (accept_focus_clicked_cb), stage);
  width = clutter_actor_get_width (rect);

  text = clutter_text_new_with_text ("Sans 16", "Try to edit this text...");
  clutter_text_set_editable (CLUTTER_TEXT (text), TRUE);
  clutter_container_add (CLUTTER_CONTAINER (stage), text, NULL);
  clutter_actor_grab_key_focus (text);
  clutter_actor_set_position (text, 10 + width, 10);  
  width += clutter_actor_get_width (rect);

  clutter_stage_set_minimum_size (CLUTTER_STAGE (stage),
                                  width,
                                  clutter_actor_get_height (rect));

  clutter_actor_show (stage);

  clutter_main ();

  return EXIT_SUCCESS;
}
