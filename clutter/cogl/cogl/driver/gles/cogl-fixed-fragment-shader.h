/*
 * Cogl
 *
 * An object oriented GL/GLES Abstraction/Utility Layer
 *
 * Copyright (C) 2008,2009 Intel Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __COGL_FIXED_FRAGMENT_SHADER_H
#define __COGL_FIXED_FRAGMENT_SHADER_H

extern const char _cogl_fixed_fragment_shader_variables_start[];
extern const char _cogl_fixed_fragment_shader_inputs[];
extern const char _cogl_fixed_fragment_shader_texturing_options[];
extern const char _cogl_fixed_fragment_shader_fogging_options[];
extern const char _cogl_fixed_fragment_shader_main_declare[];
extern const char _cogl_fixed_fragment_shader_main_start[];
extern const char _cogl_fixed_fragment_shader_fog[];
extern const char _cogl_fixed_fragment_shader_alpha_never[];
extern const char _cogl_fixed_fragment_shader_alpha_less[];
extern const char _cogl_fixed_fragment_shader_alpha_equal[];
extern const char _cogl_fixed_fragment_shader_alpha_lequal[];
extern const char _cogl_fixed_fragment_shader_alpha_greater[];
extern const char _cogl_fixed_fragment_shader_alpha_notequal[];
extern const char _cogl_fixed_fragment_shader_alpha_gequal[];
extern const char _cogl_fixed_fragment_shader_end[];

#endif /* __COGL_FIXED_FRAGMENT_SHADER_H */
