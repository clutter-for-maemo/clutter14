/*
 * Cogl
 *
 * An object oriented GL/GLES Abstraction/Utility Layer
 *
 * Copyright (C) 2008,2009 Intel Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __COGL_FIXED_VERTEX_SHADER_H
#define __COGL_FIXED_VERTEX_SHADER_H

extern const char _cogl_fixed_vertex_shader_per_vertex_attribs[];
extern const char _cogl_fixed_vertex_shader_transform_matrices[];
extern const char _cogl_fixed_vertex_shader_output_variables[];
extern const char _cogl_fixed_vertex_shader_fogging_options[];
extern const char _cogl_fixed_vertex_shader_main_start[];
extern const char _cogl_fixed_vertex_shader_frag_color_start[];
extern const char _cogl_fixed_vertex_shader_fog_start[];
extern const char _cogl_fixed_vertex_shader_fog_exp[];
extern const char _cogl_fixed_vertex_shader_fog_exp2[];
extern const char _cogl_fixed_vertex_shader_fog_linear[];
extern const char _cogl_fixed_vertex_shader_fog_end[];
extern const char _cogl_fixed_vertex_shader_end[];

#endif /* __COGL_FIXED_VERTEX_SHADER_H */
