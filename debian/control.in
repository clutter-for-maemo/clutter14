Source: clutter-1.4
Section: libs
Priority: optional
Maintainer: Aapo Rantalainen <aapo.rantalainen@gmail.com>
Build-Depends: debhelper (>= 5),
               cdbs (>= 0.4.41),
               gnome-pkg-tools,
               libgles2-sgx-img-dev | libgl1-mesa-dev | libgl-dev,
               libgtk2.0-dev,
               libcairo2-dev (>= 1.4),
               libpango1.0-dev (>= 1.20),
               libglib2.0-dev (>= 2.16),
               libxfixes-dev,
               libxdamage-dev,
               libxcomposite-dev,
               libxi-dev,
               gtk-doc-tools,
               automake1.9,
               opengles-sgx-img-common-dev [arm armel]
Standards-Version: 3.8.3

Package: libclutter-1.4
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Conflicts: libclutter-1.3-0
Replaces: libclutter-1.3-0
Description: Open GL based interactive canvas library
 Clutter is an Open GL based interactive canvas library, designed for creating
 fast, mainly 2D single window applications such as media box UIs,
 presentations, kiosk style applications and so on.

Package: libclutter-1.4-dev
Section: libdevel
Architecture: any
Depends: libclutter-1.4 (= ${binary:Version}),
         libgtk2.0-dev,
         libgles2-sgx-img-dev | libgl1-mesa-dev | libgl-dev,
         libxfixes-dev,
         libxdamage-dev,
         libxcomposite-dev,
         libxi-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: pkg-config
Suggests: libclutter-1.4-doc
Description: Open GL based interactive canvas library (development files)
 Clutter is an Open GL based interactive canvas library, designed for creating
 fast, mainly 2D single window applications such as media box UIs,
 presentations, kiosk style applications and so on.
 .
 This package contains the development files.

Package: libclutter-1.4-dbg
Section: debug
Priority: extra
Architecture: any
Depends: libclutter-1.4 (= ${binary:Version}),
         ${misc:Depends}
Description: Open GL based interactive canvas library (debug files)
 Clutter is an Open GL based interactive canvas library, designed for creating
 fast, mainly 2D single window applications such as media box UIs,
 presentations, kiosk style applications and so on.
 .
 This package contains the debug files.

Package: libclutter-1.4-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Conflicts: libclutter-0.1-doc, libclutter-doc
Replaces: libclutter-0.1-doc
Description: Open GL based interactive canvas library (documentation)
 Clutter is an Open GL based interactive canvas library, designed for creating
 fast, mainly 2D single window applications such as media box UIs,
 presentations, kiosk style applications and so on.
 .
 This package contains the documentation.
